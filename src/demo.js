import React, { useState, useRef, useEffect } from 'react';

function Test(props) {

    const [name, setName] = useState("AKASH")
    const [mobile, setMobile] = useState("8978779879")

    let referece = useRef()

    useEffect(() => {
        callRefre();
    }, [])

    const callRefre = () => {
        if (referece && referece.current) {
            referece.onClick();
        }
    }

    return (
        <div>
            <h1>{props.textValue + "Name==" + name + "  Mobile==" + mobile}</h1>
            <button ref={referece} onClick={() => { props?.showAlert("AAAAAAAAA"); setName("Ranjan"); setMobile("7889788978") }}>Submit</button>
        </div>
    )
}

export default Test;