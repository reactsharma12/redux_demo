import { ADD_BOOK,ADD_BOOK_IN_LIBRARY } from "../ActionsType"

export const addBook = (book) => {
    return async (dispatch, getState) => {
        let books = await getState().book.books;
        books = [...books, book];
        dispatch({ type: ADD_BOOK, payload: books })
    }
}

export const addBookinLibrary = (book) => {
    return async (dispatch, getState) => {
         //adding in libraryBook state & uopdate redux state of libraryBook
        let libraryBooks = await getState().book.libraryBooks;
        libraryBooks = [...libraryBooks, book];

        //removing from book state & uopdate redux state of book
        let books = await getState().book.books;
        let remainingBook=books && books?.filter((item)=>item.bookName!=book.bookName)

        dispatch({ type: ADD_BOOK_IN_LIBRARY, payload: libraryBooks })
        dispatch({ type: ADD_BOOK, payload: remainingBook })
    }
}
