import { SET_NAME } from "../ActionsType"

export const setName = (payload) => {
    return { type: SET_NAME, payload: payload }
}