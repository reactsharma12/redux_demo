import { DECREMENT_COUNTER, INCREMENT_COUNTER } from "../ActionsType"

export const increment = () => {
    return async (dispatch, getState) => {
        let countPlus = await getState().counter.count + 1;
        dispatch({ type: INCREMENT_COUNTER, payload: countPlus })
    }
}

export const decrement = (payload) => {
    return { type: DECREMENT_COUNTER, payload: payload }
}