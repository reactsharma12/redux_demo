import { SET_NAME } from "../ActionsType";

const initialState = {
    name: ''
}

export default function setName(state = initialState, data) {
    switch (data.type) {
        case SET_NAME:
            return { ...state, name: data.payload }
        default:
            return state;
    }
}