import { DECREMENT_COUNTER, INCREMENT_COUNTER } from "../ActionsType";

const initialState = {
    count: 0,
}

export default function Counter(state = initialState, data) {
    switch (data.type) {
        case INCREMENT_COUNTER:
            return { ...state, count: data.payload }
        case DECREMENT_COUNTER:
            return { ...state, count: data.payload }
        default:
            return state;
    }
}