import { combineReducers } from 'redux';
import book from './book';
import Counter from './Counter';
import setName from './setgetName';

const rootReducer = combineReducers({
    counter: Counter,
    setname: setName,
    book: book,
})

export default rootReducer;