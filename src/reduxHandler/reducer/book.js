import { ADD_BOOK, ADD_BOOK_IN_LIBRARY } from "../ActionsType";

const initialState = {
    books: [],
    libraryBooks: []
}

export default function book(state = initialState, data) {
    switch (data.type) {
        case ADD_BOOK:
            return { ...state, books: data.payload }
        case ADD_BOOK_IN_LIBRARY:
            return { ...state, libraryBooks: data.payload }
        default:
            return state;
    }
}
