import React from "react";
import { Provider } from "react-redux";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import AsyncAwaitExp from "./pages/AsyncAwaitExp";
import Category from "./pages/Category";
import Home from "./pages/Home";
import LocalstorageDemo from "./pages/LocalstorageDemo";
import ObjectsOperastion from "./pages/ObjectsOperastion";
import Orders from "./pages/Orders";
import LoanApprove from "./pages/LoanApprove";
import configureStore from './store'
const store = configureStore()

export default function App() {
  return (
    <Provider store={store}>
      <Router>
        <div>
          <nav>
            <ul>
              <li>
                <Link to="/category">Category</Link>
              </li>
              <li>
                <Link to="/order">Orders</Link>
              </li>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/promiseexp">Promise Example</Link>
              </li>
              <li>
                <Link to="/localstorageDemo">Local Storage Example</Link>
              </li>
              <li>
                <Link to="/objectsOperastion">Objects Operation</Link>
              </li>
              <li>
                <Link to="/loanApprove">Loan Approve</Link>
              </li>
            </ul>
          </nav>

          {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
          <Switch>
            <Route exact={true} path="/category" component={Category} />
            <Route exact={true} path="/order" component={Orders} />
            <Route exact={true} path="/" component={Home} />
            <Route exact={true} path="/promiseexp" component={AsyncAwaitExp} />
            <Route exact={true} path="/localstorageDemo" component={LocalstorageDemo} />
            <Route exact={true} path="/objectsOperastion" component={ObjectsOperastion} />
            <Route exact={true} path="/loanApprove" component={LoanApprove} />
          </Switch>
        </div>
      </Router>
    </Provider>
  );
}