import React, { Component } from 'react'

class LoanPlanViewDesktop extends Component {

    constructor(props) {
        super(props)
        this.state = {
            currentSelected:"firstButton"
        }
    }

    onButtonSelection = (id) => {
        this.setState({ currentSelected: id })
    }

    render() {
        const { currentSelected } = this.state;

        return (
            <div className="dt-plan-view-root">
                <div className="loan-container">
                    <p>asjkhdjdsasss</p>
                </div>
                <div className="loan-button">
                    <LoanPlanButton id="firstButton" text="Involuntary Unemployment" isSelected={currentSelected === "firstButton" ? true : false} onButtonClick={(id)=>this.onButtonSelection(id)} />
                    <LoanPlanButton id="secondButton" text="Injury/Sickness" isSelected={currentSelected === "secondButton" ? true : false} onButtonClick={(id)=>this.onButtonSelection(id)} />
                    <LoanPlanButton id="thirdButton" text="Death" isSelected={currentSelected === "thirdButton" ? true : false} onButtonClick={(id)=>this.onButtonSelection(id)} />
                    <LoanPlanButton id="fourthButton" text="Critical Illness" isSelected={currentSelected === "fourthButton" ? true : false} onButtonClick={(id)=>this.onButtonSelection(id)} />
                </div>
            </div>
        )
    }
}

export default LoanPlanViewDesktop


const LoanPlanButton = ({ id, text, isSelected, onButtonClick }) => {
    return (
        <button onClick={() => onButtonClick(id)} key={id} className="loan-btn" style={{ backgroundColor: isSelected ? '#0A214D' : '#2B9BDF' }}>
            <p className="loan-txt">{text}</p>
        </button>
    )
}


