import Raect from 'react';
import './componentStyle.css'

const InputField = ({ value, name, type, label, onHadleChange }) => {
    return (
        <>
            <p className="label">{label}:</p>
            <input
                value={value}
                name={name}
                className="input"
                type={type}
                onChange={onHadleChange}
            />
        </>
    )
}

export default InputField;