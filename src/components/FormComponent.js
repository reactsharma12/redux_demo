import React, { Component } from 'react';
import './componentStyle.css'
import InputField from './InputField';

class FormComponent extends Component {
    constructor(props) {
        super(props);
        this.state = { bookname: '', bookauthor: '', publishedYear: '' };
    }

    componentDidUpdate(prevProps, prevState) {
        console.log("=====currentBook==",prevProps.currentBook)
        if(prevProps.currentBook!=this.props.currentBook){
            this.setState({
                bookname: this.props.currentBook.bookname,
                bookauthor: this.props.currentBook.bookauthor,
                publishedYear: this.props.currentBook.publishedYear
            })
            return true;
        }
        return false;
    }

    handleValueChange = (event) => {
        switch (event.target.name) {
            case "bookname": this.setState({ bookname: event.target.value })
            case "authorname": this.setState({ bookauthor: event.target.value })
            case "publishedyear": this.setState({ publishedYear: event.target.value })
            default: console.log("======");
        }
    }

    // authorChangeHandler = (event) => {
    //     this.setState({ bookauthor: event.target.value });
    // }

    // publishedYearChangeHandler = (event) => {
    //     this.setState({ publishedYear: event.target.value });
    // }

    render() {
        return (
            <form id="create-course-form" className="form" onSubmit={(e) => {
                let filedObject = {
                    bookName: this.state.bookname,
                    author: this.state.bookauthor,
                    published: this.state.publishedYear
                }
                this.props.onSubmitForm(filedObject)
                e.preventDefault();
                document.getElementById("create-course-form").reset();
            }}>
                <h1 className="heading">ADD NEW BOOK </h1>
                <InputField value={this.state.bookname} name="bookname" type="text" label="Enter book name" onHadleChange={this.handleValueChange} />
                <InputField value={this.state.bookauthor} name="authorname" type="text" label="Enter author name" onHadleChange={this.handleValueChange} />
                <InputField value={this.state.publishedYear} name="publishedyear" type="number" label="Enter published year" onHadleChange={this.handleValueChange} />
                <br />
                <button className="form-button" type="submit">Submit</button>
            </form>
        );
    }
}

export default FormComponent;