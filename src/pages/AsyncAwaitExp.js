import { render } from '@testing-library/react'
import React, { Component } from 'react'

class AsyncAwaitExp extends Component {

    callAsyncAwait = async () => {
        return new Promise((resolve, reject) => {
            //fetch api reponse
            fetch('https://jsonplaceholder.typicode.com/todos/1')
                .then(response => response.json())
                .then(json => {
                    resolve(json);
                }).catch((error) => {
                    reject(error)
                })
            //fetch api reponse
        })
    }


    handlePropiseCall = async () => {
        try {
            let reponse = await this.callAsyncAwait();
            console.log('===reponse===,', reponse);
        } catch (error) {
            console.log("===erroero=",error);
        }

    }

    render() {
        return (
            <div>
                JAVASCRIPT DEMO
                <button onClick={() => this.handlePropiseCall()} style={{ backgroundColor: 'green' }}>Test</button>
            </div>
        )
    }
}

export default AsyncAwaitExp


const getData = () => {
    return "Hello world"
}