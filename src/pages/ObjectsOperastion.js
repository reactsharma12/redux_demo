import React, { useState } from 'react'
import FormComponent from '../components/FormComponent';
import { useSelector, useDispatch } from 'react-redux';
import './common.css';
import { addBook, addBookinLibrary } from '../reduxHandler/actions/bookAction';

const books = [
    {
        bookName: "Physics1",
        author: "H. C verma",
        published: 2005
    },
    {
        bookName: "Chemistry",
        author: "Pradeep",
        published: 2008
    },
    {
        bookName: "Math",
        author: "R. S agarwal",
        published: 2010
    },
    {
        bookName: "Hindi",
        author: "Ramdhari singh dinkar",
        published: 2007
    },
    {
        bookName: "English",
        author: "Steve jobs",
        published: 2006
    },
]

const ObjectsOperastion = (props) => {
    const dispatch = useDispatch();

    const { books, libraryBooks } = useSelector(state => state.book)  //state: redux all states, book:reducer name defined in root reducer, books:reducer state that defined in initialState

    const [currentBook, setcurrentBook] = useState()



    const getAllFieldsValue = (values) => {
        dispatch(addBook(values));//always pass an action in dispatch
        console.log("===fiedl values===", values);
    }

    const addBookToLibrary = (book) => {
        dispatch(addBookinLibrary(book))
    }


    return (
        <>
            <div className="root">
                <FormComponent currentBook={currentBook} onSubmitForm={(values) => getAllFieldsValue(values)} />
            </div>
            <div className="root">
                <div className="container">
                    {books && books.map((item, index) => (
                        <div className="item-container">
                            <div style={{ display: 'flex',justifyContent:'flex-end' }}>
                                <a className="edit-btn" onClick={() => setcurrentBook(item)}>Edit </a>
                            </div>
                            <p>Name: {item.bookName}</p>
                            <p>Author: {item.author}</p>
                            <p>Published: {item.published}</p>
                            <div className="btn-container">
                                <button onClick={() => addBookToLibrary(item)} className="button">Add to library</button>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
            <div className="root">
                <div className="container">
                    {libraryBooks && libraryBooks.map((item, index) => (
                        <div className="item-container">
                            <p>Name: {item.bookName}</p>
                            <p>Author: {item.author}</p>
                            <p>Published: {item.published}</p>
                            <div className="btn-container">
                                <button className="button">Add to library</button>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </>
    )
}

export default ObjectsOperastion
