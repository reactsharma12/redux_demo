import React, { useState } from 'react'
import LoanPlanViewDesktop from '../components/LoanPlanViewDesktop';
import LoanPlanView from '../components/LoanPlanView'
import './common.css';

const loanPlans = [
    {
        id: 1,
        title: `Involuntary Unemployment`,
        subTitle1:`Layoff (without cause)`,
        subTitle2:`If you have been employed for at least 90 days and are eligible for EI benefits`,
        subTitle3:`Benefit`,
        description: `       - Payment made for up to 6 months while you are involuntarily unemployed. 
       - PLUS benefit payment will include 1/24th of the principle balance due on the date of claim.
       - If you are still involuntarily unemployed after 6 months we will make an additional payment equal to or lesser to your remaining balance and $2,000.`
    },
    {
        id: 2,
        title: `Injury/Sickness`,
        subTitle1:`Injury/Sickness/Fracture`,
        subTitle2:`Where you were unable to work for 10 consecutive days`,
        subTitle3:`Benefit`,
        description: `       - Payments made for up to 6 months while you are involuntarily unemployed.
       - PLUS benefit payment will include 1/24th of the principle balance due on the date of claim.
       - If you are still unable to work due to injury or sickness after 6 months we will make an additional payment equal to or lesser to your remaining balance and `
    },
    {
        id: 3,
        title: `Death`,
        subTitle1:`Borrower passes away`,
        subTitle2:``,
        subTitle3:`Benefit`,
        description: `       - 100% payment, balance paid in full. (maximum benefit $15,000)`
    },
    {
        id: 4,
        title: `Critical Illness`,
        subTitle1:`Critical Diagnosis`,
        subTitle2:`Your are diagnosed with the life-threatening cancer, suffer a stroke, heart attack, kidney failure or a major organ transplant for the first time in your life`,
        subTitle3:`Benefit`,
        description: `       - 100% payment, balance paid in full. (maximum benefit $15,000)`
    }
]

function LoanApprove() {

    const [currentItem, setCurrentItem] = useState()

    const onToogle = (item) => {
        if (item.id == currentItem?.id) {
            setCurrentItem('')
        } else {
            setCurrentItem(item);
        }
    }

    return (
        <div className="root">
            <div className="loan-root">
                <div className="title">
                    <h4 style={{display:'flex',justifyContent:'center',textAlign:'center'}}>Congratulation you are approved for an intsallment loan</h4>
                </div>
                <div className="subtitle">
                    <h4 style={{display:'flex',justifyContent:'center',textAlign:'center'}}>Protecting your new loan is just as important as receiving it.</h4>
                </div>
                    <div className="loan-view-container">
                        {loanPlans.map((item, i) => {
                            return (
                                <LoanPlanView currentItem={currentItem} item={item} onSelect={onToogle} />
                            )
                        })}
                    </div>
            </div>
        </div>
    )
}

export default LoanApprove
