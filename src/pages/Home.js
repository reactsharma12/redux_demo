import React from 'react'
import { connect, useDispatch, useSelector } from 'react-redux'
import { decrement, increment } from '../reduxHandler/actions/counter'
import { setName } from '../reduxHandler/actions/setname'

function Home(props) {

    const dispatch = useDispatch();
    const count = useSelector(state => state.counter.count);
    const name = useSelector(state => state.setname.name);

    return (
        <div>
            <h2>Redux dmeo</h2>
            <p>Counter: {count + "      Name: " + name}</p>
            <div style={{ marginTop: 50, alignItems: 'center' }}>
                <button onClick={() => dispatch(increment())} style={{ backgroundColor: 'red' }}>Click Increment</button>
                <button onClick={() => dispatch(decrement(count - 1))} style={{ backgroundColor: 'yellow' }}>Click Decremenet</button>
                <button onClick={() => dispatch(setName("Ranjan"))} style={{ backgroundColor: 'green' }}>Set Name</button>
            </div>
        </div>
    )
}

// const mapStateToProps = ({ counter, setname }) => {
//     return {
//         count: counter.count,
//         name: setname.name,
//     }
// }

// const mapDispatchToProps = (dispatch) => {
//     return {
//         increment: () => dispatch(increment()),
//         decrement: (data) => dispatch(decrement(data)),
//         setName: (data) => dispatch(setName(data))
//     }
// }

// export default connect(mapStateToProps,mapDispatchToProps)(Home);
export default Home;

